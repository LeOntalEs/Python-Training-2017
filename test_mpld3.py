import mpld3
import numpy as np
import matplotlib.pyplot as plt

X = np.arange(20, step=0.1)
Y = np.sin(X)
fig, ax = plt.subplots(1, 1)
ax.plot(X, Y, 'r', lw=3)
ax.plot(X, np.zeros(X.shape), 'k')
s = mpld3.fig_to_html(fig)
# print(s)
html = """
<html>
    <head>
        <title>Test MPLD3 </title>
    </head>
    <body>
            <h1> This is Sin Graph </h1>
            <hr>
            %s
    </body>
</html>"""%s
with open('test_mpld3.html', 'w+') as fp:
    fp.write(html)