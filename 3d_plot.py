import numpy as np
import matplotlib.pyplot as plt

# fig = plt.figure()
# ax = fig.gca(projection='3d')

from mpl_toolkits.mplot3d import  Axes3D 
fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(100)+2
Y = np.arange(100)*2
Z = np.arange(100)**2
ax.plot(X, Y, Z, 'r', lw=5)
plt.show()