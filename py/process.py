# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 12:27:13 2017

@author: taratep
"""
all_stud = dict()
with open('dataset.csv', 'r') as fp:
    lines = fp.read().split('\n')
    for line in lines[1:]:
        elem = line.split(',')
        all_stud[elem[0]] = [float(x.strip()) for x in elem[1:]]

total_scores = dict()
for k, v in all_stud.items():
    total_scores[k] = sum(v)
    
total_lst = [total_scores[k] for k in total_scores]
below = list()
above = list()

avg = sum(total_lst) / len(total_lst)
for k, v in total_scores.items():
    if v <= avg: below.append((k, v))
    else: above.append((k, v))

print(avg)    
print(below)
print()
print(above)
    