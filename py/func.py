# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 12:14:29 2017

@author: taratep
"""

def f(x):
    return x**2

def exponent(base=1, expo=2):
    res = base
    for i in range(expo-1):
        res = res*base
    return res
        
def mys(a):
    a.append(4)
    
def createlist(a):
    lst = [x for x in range(a)]
    return lst

def func1():
    def func2(x):
        return 5*x
    return func2

def mys2(a=None):
    if a is None:
        a = list()
    a.append(2)
    return a

print(f(2))
print(exponent(5, 5))

a = []
mys(a)
print(a)

a = createlist(8)
print(a)

fx = func1()
print(fx(5))

a = mys2()
print(a)
a = mys2()
print(a)
