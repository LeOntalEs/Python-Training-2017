# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 10:52:36 2017

@author: taratep
"""

import os

x = int(float(input('Input x: ')))
if (x%2 == 0):
    print('even')
elif x%2 == 1:
    print('odd')
else:
    print('else')
    
print( 'even' if x%2==0 else 'odd')
    
a = [1,2,3]
a = []
if len(a) != 0:
    print('has element')
else:
    print('empty')
