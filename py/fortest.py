# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 11:12:18 2017

@author: taratep
"""

lst = [1, 2, 3]
print(sum(lst))
print(min(lst))
print(max(lst))
print(sum(lst)/len(lst))

print()
for i, x in enumerate(lst):
    print(i, x)

print()
ran = range(10, 0, -1)
print(list(ran))
for i in ran:
    print(i)
print()

d = {'a':1, 'b':2}
for k, v in d.items():
    print(k, v)

print()
lst = list(range(5))
i = 0
while lst:
    print(lst.pop())
    
lst = [ran]

    