def grading(score):
    if score > 80:
        grade = 'A'
    elif 70 <= score < 80:
        grade = 'B'
    elif 60 <= score < 70:
        grade = 'C'
    elif 50 <= score < 60:
        grade = 'D'
    else: 
        grade = 'F'
    return grade

student = dict()
with open('dataset.csv', 'r') as fp:
    lines = fp.read().split('\n')
    for line in lines[1:]:
        elem = line.split(',')
        student[elem[0]] = [float(x) for x in elem[1:]]

all_score = dict()
for sid, score in student.items():
    print(sid, score)
    sumscore = sum(score)
    all_score[sid] = sumscore, grading(sumscore)
print(all_score)

