from collections import Counter

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def grading(score):
    if score > 80:
        grade = 'A'
    elif 70 <= score < 80:
        grade = 'B'
    elif 60 <= score < 70:
        grade = 'C'
    elif 50 <= score < 60:
        grade = 'D'
    else: 
        grade = 'F'
    return grade

df = pd.read_csv('dataset.csv')
columns = list(df.columns)
columns.remove('Student ID')
df['Total'] = df[columns].sum(axis=1)
df['Grade'] = [grading(x) for x in df['Total']]
columns = list(df.columns)
columns.remove('Student ID')
print(df[columns].describe(include='all'))
print(df.Total)
df[['Total', 'Midterm']].hist(grid=False)
# df2 = df.copy()
# plt.figure(1)
# pd.value_counts(df2.Grade).plot(kind='bar')
# plt.figure(2)
plt.show()
